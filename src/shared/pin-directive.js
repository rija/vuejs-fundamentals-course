export default function (element, binding) {
  element.style.position = 'absolute';
  Object.keys(binding.value).forEach((position) => {
    element.style[position] = binding.value[position];
  });
}

// function setPadding(element, binding) {
//   element.style.position = 'absolute';
//   Object.keys(binding.value).forEach((position) => {
//     element.style[position] = binding.value[position];
//   });
// }
//
// export default {
//   beforeMount(element, binding) {
//     setPadding(element, binding);
//   },
//   updated(element, binding) {
//     setPadding(element, binding);
//   },
// };
