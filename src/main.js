import { createApp } from 'vue';
import router from './router';
import store from './store';
import App from './App.vue';
import pinDirective from './shared/pin-directive';
import currencyFilter from './shared/currency-filter';

const app = createApp(App);
app.provide('semVersion', '1.0.0-alpha');
app.config.globalProperties.$filters = {
  currency: currencyFilter,
};

app
  .use(router)
  .use(store)
  .directive('pin', pinDirective)
  .mount('#app');
